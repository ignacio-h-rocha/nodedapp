
import { CellsElement } from '@cells/cells-element';
import { html } from 'lit-element';
import '@bbva-web-components/bbva-spinner';


/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/
export class PtSpinner extends CellsElement {
  static get is() {
    return 'pt-spinner';
  }

  // Declare properties
  static get properties() {
    return {
      show: {type: Boolean}
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.show = false;
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-spinner', data => this._toggleSPinner(data));
  }

  _toggleSPinner(data){
    this.show = data;
  }

  render() {
    return html`
      <style>
        .fullSpiner {
          z-index: 999;
          width: 100vw;
          height: 100vh;
          background: rgba(55,155,255,0.5);
          left: 0;
          top: 0;
          position: absolute;
          display: flex;
          justify-content: center;
        }
      </style>
      
      
      ${
        this.show ? 
        html`<div class="fullSpiner">
              <bbva-spinner> </bbva-spinner>
            </div>` :
        html``
      }
        
    `;
  }

}

// Register the element with the browser
customElements.define(PtSpinner.is, PtSpinner);
