
import { CellsElement } from '@cells/cells-element';
import { html } from 'lit-element';
import '@cells-components/cells-generic-dp';
import { strict } from 'assert';


/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/
export class DmPractitioner extends CellsElement {
  static get is() {
    return 'dm-practitioner';
  }

  // Declare properties
  static get properties() {
    return {
      nextPagePokemones: {type: String}
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.nextPagePokemones = 'https://pokeapi.co/api/v2/pokemon';
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-login', data => this._login(data));
    this.subscribe('ch-create-user', data => this._createUser(data));
    this.subscribe('ch-get-user-info', _ => this._getUserData());
    this.subscribe('ch-get-user-movements', _ => this._getUserMovements());
    this.subscribe('ch-get-pokemones', _ => this._getPokemones());
    this.subscribe('ch-buy-pokemon', body => this._buyPokemon(body));
    this.subscribe('ch-get-user-pokemones', _ => this._getUserPokemones());
  }

  _getUserPokemones() {
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
    dp.method = "GET";
    dp.headers = {
      token: sessionStorage.getItem('token')
    };
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200) {
          let newData = detail.data.map( movement => {
            return {
              name: movement.details.name,
              image: movement.details.image,
            }
          });
          let newDetail = {
            statusCode: 200,
            data: newData
          }
          this.publish("ch-get-user-pokemones-response", newDetail);
        } else {
          this.publish("ch-get-user-pokemones-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-get-user-pokemones-response", {
          data: "error"
        });
      });
  }

  _buyPokemon(body){
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
    dp.method = "POST";
    dp.headers = {
      token: sessionStorage.getItem('token')
    };
    dp.body = {
      data: body
    }
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200 || detail.statusCode == 204) {
          this.publish("ch-buy-pokemon-response", detail);
        } else {
          this.publish("ch-buy-pokemon-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-buy-pokemon-response", {
          data: "error"
        });
      });
  }

  _getPokemones(){
    let dp = this.shadowRoot.getElementById('dpPokemones');
    dp.host = this.nextPagePokemones;
    dp.method = "GET";
    dp.generateRequest()
      .then((result) => {
        this.nextPagePokemones = result.response.next;
        this._getFullPokemonData(result.response.results);
      })
      .catch((error) => {
        this.publish("ch-get-pokemones-response", {
          data: "error"
        });
      });
  }

  _getFullPokemonData(list){
    let dp = this.shadowRoot.getElementById('dpPokemones');
    dp.method = "GET";
    list.map(pokemon => {
      dp.host = pokemon.url;
      dp.generateRequest()
      .then((result) => {
        var pokemon = {
          name: result.response.name,
          price: result.response.weight,
          image: result.response.sprites.front_default || result.response.sprites.front_female || result.response.sprites.front_shiny
        }
        this.publish("ch-get-pokemones-response", pokemon);
      });
    })
  }

  _getUserMovements(){
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
    dp.method = "GET";
    dp.headers = {
      token: sessionStorage.getItem('token')
    };
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200) {
          let newData = detail.data.map( movement => {
            let d = new Date(movement.date);
            let dateForm = `${d.getDay()}/${d.getMonth() + 1}/${d.getFullYear()} - 
            ${d.getHours()}:${d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()}`;
            return {
              amount: movement.amount,
              date: dateForm,
              details: movement.details,
              type: movement.type
            }
          });
          let newDetail = {
            statusCode: 200,
            data: newData
          }
          this.publish("ch-get-user-movements-response", newDetail);
        } else {
          this.publish("ch-get-user-movements-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-get-user-movements-response", {
          data: "error"
        });
      });
  }

  _getUserData() {
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "users/" + sessionStorage.getItem('user');
    dp.method = "GET";
    dp.headers = {
      token: sessionStorage.getItem('token')
    };
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200) {
          this.publish("ch-get-user-info-response", detail);
        } else {
          this.publish("ch-get-user-info-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-get-user-info-response", {
          data: "error"
        });
      });
  }

  _createUser(data){
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "users"
    dp.method = "POST";
    dp.body = {
      user: data.user,
      password: data.pass
    };
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200) {
          this.publish("ch-create-user-response", detail);
        } else {
          this.publish("ch-create-user-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-create-user-response", {
          data: "error"
        });
      });
  }

  _login(data) {
    let dp = this.shadowRoot.getElementById('dp');
    dp.path = "login"
    dp.method = "POST";
    dp.body = {
      user: data.user,
      password: data.pass
    };
    dp.generateRequest()
      .then((result) => {
        var detail = result.response;
        if (detail.statusCode == 200) {
          this.publish("ch-login-response", detail);
        } else {
          this.publish("ch-login-response", {
            data: "error"
          });
        }
      })
      .catch((error) => {
        this.publish("ch-login-response", {
          data: "error"
        });
      });
  }

  render() {
    return html`
      <cells-generic-dp id="dp" host="https://obyi7eedtc.execute-api.us-west-2.amazonaws.com/alpha"> </cells-generic-dp>
      <cells-generic-dp id="dpPokemones" method="GET"> </cells-generic-dp>
    `;
  }

}

// Register the element with the browser
customElements.define(DmPractitioner.is, DmPractitioner);
