
import { CellsElement } from '@cells/cells-element';
import { html } from 'lit-element';
import '@bbva-web-components/bbva-button-default';


/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/
export class PtInfoLabel extends CellsElement {
  static get is() {
    return 'pt-info-label';
  }

  // Declare properties
  static get properties() {
    return {
      msg: {type: String},
      show: {type: Boolean}
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.msg = "";
    this.show = false;
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-msg-label', data => this._show(data));
  }

  _show(data){
    this.show = true;
    this.msg = data;
  }

  _hide(){
    this.show = false;
    this.msg = "";
  }

  render() {
    return html`
      <style>
        .fullSpiner {
          z-index: 998;
          width: 100vw;
          height: 100vh;
          background: rgba(255,255,255,0.9);
          left: 0;
          top: 0;
          position: absolute;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
        }
      </style>

      ${
        this.show ? 
        html`<div class="fullSpiner">
              <h3>${this.msg}</h3>
              <bbva-button-default @click="${this._hide}">Cerrar</bbva-button-default>
            </div>` :
        html``
      }
        
    `;
  }

}

// Register the element with the browser
customElements.define(PtInfoLabel.is, PtInfoLabel);
