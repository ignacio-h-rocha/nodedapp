import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import styles from './userpokemones-page-styles.js';


class UserpokemonesPage extends CellsPage {
  static get is() {
    return 'userpokemones-page';
  }

  constructor() {
    super();
    this.headerTitle = 'Mis pokemon';
    this.pokemones = [];
  }

  static get properties() {
    return {
      headerTitle: { type: String },
      pokemones: { type: Array },
    };
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-get-user-pokemones-response', data => this._getUserPokemones(data));
  }

  _getUserPokemones(pokemones) {
    if (pokemones.data == "error") {
      this.publish('ch-msg-label', "Error inesperado");
    } else {
      this.pokemones = pokemones.data;
    }
    this.publish('ch-spinner', false);
  }

  onPageEnter() {
    this.publish('ch-spinner', true);
    this.publish('ch-get-user-pokemones');
  }

  _logout() {
    this.navigate('home')
  }

  _goMovements(){
    this.navigate("movements");
  }

  render() {
    return html`
    <style>${this.constructor.shadyStyles}</style>
    <link rel="stylesheet" href="../../resources/css/bootstrap4.3.1.min.css">
    <cells-template-paper-drawer-panel mode="seamed">

      <div slot="app__header">
        <bbva-header-main text=${this.headerTitle}></bbva-header-main>
        <practitioner-datamanagers id="id"></practitioner-datamanagers>
      </div>

      <div slot="app__main">
        <div class="container">

          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-info btn-lg btn-block" @click="${this._goMovements}">Ver movimientos</button>
            </div>
          </div>

          <br><br>

          <div class="row justify-content-center">
            <h1 style="color: green">Mis pokemones</h1>
          </div>

          <div class="row justify-content-center">

          ${this.pokemones.map(pokemon => {
      return html`
            <div class="col-10 col-md-3" style="padding-bottom: 1rem;" >
              <div style="border: solid gray 2px">
                <div class="col-12" style="text-align: center">
                  <div style="display: flex; width: 100%; justify-content: center;">
                    <div style="width: 96px; height: 96px; background: url(${pokemon.image})"></div>
                  </div>
                  <br>
                  <h3>${pokemon.name}</h3>
                </div>
                <br>
              </div>
            </div>
            `
    })}
            
          </div>

          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-danger btn-lg btn-block" @click="${this._logout}">Salir</button>
            </div>
          </div>

            
        </div>
        
      </div>

     </cells-template-paper-drawer-panel>`;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
    `;
  }
}

window.customElements.define(UserpokemonesPage.is, UserpokemonesPage);

