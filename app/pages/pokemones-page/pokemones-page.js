import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import styles from './pokemones-page-styles.js';


class PokemonesPage extends CellsPage {
  static get is() {
    return 'pokemones-page';
  }

  constructor() {
    super();
    this.headerTitle = 'Tienda pokemon';
    this.balance = 0;
    this.pokemones = [];
    this.tempPokemones = [];
    this.count = 0;
  }

  static get properties() {
    return {
      balance: { type: Number },
      count: { type: Number },
      headerTitle: { type: String },
      pokemones: { type: Array },
      tempPokemones: { type: Array },
    };
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-get-user-info-response', data => this._getUserInfo(data));
    this.subscribe('ch-get-pokemones-response', data => this._getPokemones(data));
    this.subscribe('ch-buy-pokemon-response', data => this._buyPokemonResponse(data));
  }

  _buyPokemonResponse(data){
    if (data.data == "error") {
      this.publish('ch-msg-label', "Error inesperado");
    } else if (data.statusCode == 204){
      this.publish('ch-msg-label', "Fondos insuficientes");
    } else {
      this.publish('ch-msg-label', "Compra exitosa");
      this.publish('ch-get-user-info');
    }
    this.publish('ch-spinner', false);
  }

  _getUserInfo(data) {
    if (data.data == "error") {
      this.publish('ch-msg-label', "Error inesperado");
    } else {
      this.balance = data.data;
    }
  }

  _getPokemones(data) {
    if (this.count < 19) {
      this.tempPokemones.push(data);
      this.count++;
    } else {
      this.pokemones.map(pokemon => {
        this.tempPokemones.unshift(pokemon);
      });
      this.pokemones = this.tempPokemones;
      this.tempPokemones = [];
      this.count = 0;
      this.publish('ch-spinner', false);
    }
  }

  _buy(pokemon) {
    let date = new Date();
    let body = {
      "amount": pokemon.price,
      "type": "Compra",
      "details": pokemon,
      "date": date.getTime()
    }
    this.publish('ch-buy-pokemon', body);
    this.publish('ch-spinner', true);
  }

  _getMorePokemones(){
    this.publish('ch-spinner', true);
    this.publish('ch-get-pokemones');
  }

  onPageEnter() {
    this.publish('ch-spinner', true);
    this.publish('ch-get-user-info');
    this.publish('ch-get-pokemones');
  }

  _logout() {
    this.navigate('home')
  }

  _goMovements(){
    this.navigate("movements");
  }

  render() {
    return html`
    <style>${this.constructor.shadyStyles}</style>
    <link rel="stylesheet" href="../../resources/css/bootstrap4.3.1.min.css">
    <cells-template-paper-drawer-panel mode="seamed">

      <div slot="app__header">
        <bbva-header-main text=${this.headerTitle}></bbva-header-main>
        <practitioner-datamanagers id="id"></practitioner-datamanagers>
      </div>

      <div slot="app__main">
        <div class="container">

          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-info btn-lg btn-block" @click="${this._goMovements}">Ver movimientos</button>
            </div>
          </div>

          <br><br>

          <div class="row justify-content-center">
            <h1 style="color: green">$${this.balance}</h1>
          </div>

          <div class="row justify-content-center">

          ${this.pokemones.map(pokemon => {
      return html`
            <div class="col-10 col-md-3" style="padding-bottom: 1rem;" >
              <div style="border: solid gray 2px">
                <div class="col-12" style="text-align: center">
                  <div style="display: flex; width: 100%; justify-content: center;">
                    <div style="width: 96px; height: 96px; background: url(${pokemon.image})"></div>
                  </div>
                  <br>
                  <h3>${pokemon.name}</h3>
                </div>
                <br>
                <div class="row justify-content-center">
                  <div class="col-7">
                    <button type="button" class="btn btn-success btn-lg btn-block" @click="${()=>this._buy(pokemon)}">Comprar $${pokemon.price}</button>
                  </div>
                </div>
                <br>
              </div>
            </div>
            `
    })}
            
          </div>

          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-info btn-lg btn-block" @click="${this._getMorePokemones}">Mas pokemones</button>
              <br><br>
              <button type="button" class="btn btn-danger btn-lg btn-block" @click="${this._logout}">Salir</button>
            </div>
          </div>

            
        </div>
        
      </div>

     </cells-template-paper-drawer-panel>`;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
    `;
  }
}

window.customElements.define(PokemonesPage.is, PokemonesPage);

