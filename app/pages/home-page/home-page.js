import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import styles from './home-page-styles.js';


class HomePage extends CellsPage {
  static get is() {
    return 'home-page';
  }

  constructor() {
    super();
    this.headerTitle = 'Inicia sesion';
  }

  static get properties() {
    return {
      user: { type: String },
      password: { type: String },
      headerTitle: { type: String },

    };
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-login-response', data => this._loginResponde(data));
  }

  _loginResponde(data){
    if (data.data == "error" ) {
      this.publish('ch-msg-label', "Datos incorrectos");
    } else {
      sessionStorage.setItem('user', data.user);
      sessionStorage.setItem('token', data.detail);
      this.navigate('movements');
    }
    this.publish('ch-spinner', false);
  }

  onPageEnter() {
    //console.log('Page loaded');
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('token');
    this.user = '';
    this.password = '';
    this.shadowRoot.getElementById('usr').value='';
    this.shadowRoot.getElementById('pss').value='';

  }

  onPageLeave() {
    //console.log('Page unloaded');
  }

  _singIn() {
    this.navigate('singin', {});
  }

  _login() {
    //console.log(this.user, this.password)
    let user = this.user,
      pass = this.password;
    this.publish('ch-login', { user, pass });
    this.publish('ch-spinner', true);
    
    //this.navigate('another', { title: 'This is another page' });
  }

  handleUser(e) {
    this.user = e.target.value;
    //this.shadowRoot.getElementById('id').getAlert(e.target.value);
  }

  handlePass(e) {
    this.password = e.target.value;
  }

  render() {
    return html`
    <style>${this.constructor.shadyStyles}</style>
    <link rel="stylesheet" href="../../resources/css/bootstrap4.3.1.min.css">
    <cells-template-paper-drawer-panel mode="seamed">

      <div slot="app__header">
        <bbva-header-main text=${this.headerTitle}></bbva-header-main>
        <practitioner-datamanagers id="id"></practitioner-datamanagers>
      </div>

      <div slot="app__main">
        <div class="container">

          <div class="row justify-content-center">
            <div class="col-9">
              <input type="text" id="usr" class="form-control" @input=${this.handleUser} placeholder="Usuario">
            </div>
            <div class="col-9 d-sm-none">&nbsp;</div>
            <div class="col-9">
              <input type="password" id="pss" class="form-control" @input=${this.handlePass} placeholder="Contraseña">
            </div>
          </div>
          <hr>
          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-success btn-lg btn-block" @click="${this._login}">Login</button>
              <br>
              <button type="button" class="btn-block btn-link" @click="${this._singIn}">Registrate gratis</button>
            </div>
          </div>
        </div>
        
      </div>

     </cells-template-paper-drawer-panel>`;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
    `;
  }
}

window.customElements.define(HomePage.is, HomePage);

