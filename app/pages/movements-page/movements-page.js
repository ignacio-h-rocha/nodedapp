import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import styles from './movements-page-styles.js';


class MovementsPage extends CellsPage {
  static get is() {
    return 'movements-page';
  }

  constructor() {
    super();
    this.headerTitle = 'Movimientos';
    this.balance = 0;
    this.movements = [];
  }

  static get properties() {
    return {
      headerTitle: { type: String },
      balance: { type: Number },
      movements: { type: Array }
    };
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-get-user-info-response', data => this._getUserInfo(data));
    this.subscribe('ch-get-user-movements-response', data => this._getUserMovements(data));
  }

  _getUserInfo(data) {
    if (data.data == "error") {
      this.publish('ch-msg-label', "Error inesperado");
    } else {
      this.balance = data.data;
    }
  }

  _getUserMovements(movements) {
    if (movements.data == "error") {
      this.publish('ch-msg-label', "Error inesperado");
    } else {
      this.movements = movements.data;
    }
    this.publish('ch-spinner', false);
  }

  onPageEnter() {
    //console.log('Page loaded');
    this.publish('ch-spinner', true);
    this.publish('ch-get-user-info');
    this.publish('ch-get-user-movements');
  }

  onPageLeave() {
    //console.log('Page unloaded');
  }

  _return() {
    this.navigate('home', {});
  }

  _singin() {
    console.log(this.user, this.password, this.password2)
    //this.publish('from-channel', { from: 'home' });
    //this.navigate('another', { title: 'This is another page' });
  }

  handleUser(e) {
    this.user = e.target.value;
    //this.shadowRoot.getElementById('id').getAlert(e.target.value);
  }

  handlePass(e) {
    this.password = e.target.value;
  }

  handlePass2(e) {
    this.password2 = e.target.value;
  }

  getDate(time){
    let d = new Date();
    return `${d.getDay(time)}/${d.getMonth(time) + 1}/${d.getFullYear(time)} - 
            ${d.getHours(time)}:${d.getMinutes(time) < 10 ? '0' + d.getMinutes(time) : d.getMinutes(time)}`;
  }

  _logout(){
    this.navigate('home')
  }

  _goList(){
    this.navigate('pokemones')
  }

  _goMyPokemones(){
    this.navigate('userpokemones')
  }

  render() {
    return html`
    <style>${this.constructor.shadyStyles}</style>
    <link rel="stylesheet" href="../../resources/css/bootstrap4.3.1.min.css">
    <cells-template-paper-drawer-panel mode="seamed">

      <div slot="app__header">
        <bbva-header-main text=${this.headerTitle}></bbva-header-main>
        <practitioner-datamanagers id="id"></practitioner-datamanagers>
      </div>

      <div slot="app__main">
        <div class="container">

          <div class="row justify-content-center">
            <div class="col-4">
              <button type="button" class="btn btn-info btn-lg btn-block" @click="${this._goList}">Comprar pokemones</button>
            </div>
            <div class="col-4">
              <button type="button" class="btn btn-info btn-lg btn-block" @click="${this._goMyPokemones}">Mis pokemones</button>
            </div>
          </div>

          <br><br>

          <div class="row justify-content-center">
            <h1 style="color: green">$${this.balance}</h1>
          </div>

          ${this.movements.map(movement => {
            return html `
              <div class="row justify-content-center">
                <div class="col-10" style="border: solid gray 1.5px">
                  <div class="col-12">
                    <h4>${movement.type}</h4>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      Fecha
                    </div>
                    <div class="col-6">
                      ${movement.date}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      Monto
                    </div>
                    <div class="col-6">
                      $${movement.amount}
                    </div>
                  </div>
                </div>
              </div>
              <br><br>
            `
          })}

          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-danger btn-lg btn-block" @click="${this._logout}">Salir</button>
            </div>
          </div>

            
        </div>
        
      </div>

     </cells-template-paper-drawer-panel>`;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
    `;
  }
}

window.customElements.define(MovementsPage.is, MovementsPage);

