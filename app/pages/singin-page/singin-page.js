import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-header-main';
import styles from './singin-page-styles.js';


class SinginPage extends CellsPage {
  static get is() {
    return 'singin-page';
  }

  constructor() {
    super();
    this.headerTitle = 'Registrate gratis';
  }

  static get properties() {
    return {
      user: { type: String },
      password: { type: String },
      password2: { type: String },
      headerTitle: { type: String },

    };
  }

  firstUpdated() {
    super.firstUpdated();
    this._handleConnections();
  }

  _handleConnections() {
    this.subscribe('ch-create-user-response', data => this._createUserResponse(data));
  }

  onPageEnter() {
    //console.log('Page loaded');
  }

  onPageLeave() {
    //console.log('Page unloaded');
  }

  _return() {
    this.navigate('home', {});
  }

  _createUserResponse(data) {
    if (data.data === "error") {
      this.publish('ch-msg-label', "El usuario " + this.user + " ya existe");
    } else {
      this.publish('ch-msg-label', "Usuario creado con exito");
      this.navigate('home', {});
    }
    this.publish('ch-spinner', false);
  }

  _singin() {
    if (this.password === this.password2) {
      this.publish('ch-spinner', true);
      this.publish('ch-create-user', { user: this.user, pass: this.password });
    } else {
      this.publish('ch-msg-label', "Las contraseñas no coinciden");
    }
  }

  handleUser(e) {
    this.user = e.target.value;
    //this.shadowRoot.getElementById('id').getAlert(e.target.value);
  }

  handlePass(e) {
    this.password = e.target.value;
  }

  handlePass2(e) {
    this.password2 = e.target.value;
  }

  render() {
    return html`
    <style>${this.constructor.shadyStyles}</style>
    <link rel="stylesheet" href="../../resources/css/bootstrap4.3.1.min.css">
    <cells-template-paper-drawer-panel mode="seamed">

      <div slot="app__header">
        <bbva-header-main text=${this.headerTitle}></bbva-header-main>
        <practitioner-datamanagers id="id"></practitioner-datamanagers>
      </div>

      <div slot="app__main">
        <div class="container">

          <div class="row justify-content-center">
            <div class="col-9">
              <input type="text" class="form-control" @input=${this.handleUser} placeholder="Usuario">
            </div>
            <div class="col-9 d-sm-none">&nbsp;</div>
            <div class="col-9">
              <input type="password" class="form-control" @input=${this.handlePass} placeholder="Contraseña">
            </div>
            <div class="col-9 d-sm-none">&nbsp;</div>
            <div class="col-9">
              <input type="password" class="form-control" @input=${this.handlePass2} placeholder="Repetir contraseña">
            </div>
          </div>
          <hr>
          <div class="row justify-content-center">
            <div class="col-8">
              <button type="button" class="btn btn-success btn-lg btn-block" @click="${this._singin}">Registrarse</button>
              <br>
              <button type="button" class="btn-block btn-link" @click="${this._return}">Regresar</button>
            </div>
          </div>
        </div>
        
      </div>

     </cells-template-paper-drawer-panel>`;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
    `;
  }
}

window.customElements.define(SinginPage.is, SinginPage);

