(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'singin': '/singin',
      'userpokemones': '/userpokemones',
      'pokemones': '/pokemones',
      'movements': '/movements',
    }
  });
}(document));
