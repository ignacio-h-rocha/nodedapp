/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"lit-initial-components-._app_e": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "scripts/lit";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./app/scripts/lit-initial-components.js","vendors-lit-initial-components-.._.._.nvm_versions_node_v9.11.2_lib_node_modules_@cells_cells-cli_no-bc5f4b59","vendors-lit-initial-components-._node_modules_@bbva-web-components_bbva-b","vendors-lit-initial-components-._node_modules_@cells-","vendors-lit-initial-components-._node_modules_@cells-components_cells-a","vendors-lit-initial-components-._node_modules_@cells-components_cells-f","vendors-lit-initial-components-._node_modules_@cells-components_cells-l","vendors-lit-initial-components-._node_modules_@cells-components_cells-lit-helpers_c","vendors-lit-initial-components-._node_modules_@cells-components_cells-p","vendors-lit-initial-components-._node_modules_@webcomponents_shadycss_e","vendors-lit-initial-components-._node_modules_@webcomponents_shadycss_src_s","vendors-lit-initial-components-._node_modules_lit-element_lib","vendors-lit-initial-components-._node_modules_lit-html_d","vendors-lit-initial-components-._node_modules_r","vendors-lit-initial-components-._node_modules_rxjs__esm5_a","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_observable_C","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_operators_a","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_operators_c","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_operators_r","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_operators_t","vendors-lit-initial-components-._node_modules_rxjs__esm5_internal_sc"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/elements/dm-practitioner.js":
/*!*****************************************!*\
  !*** ./app/elements/dm-practitioner.js ***!
  \*****************************************/
/*! exports provided: DmPractitioner */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DmPractitioner", function() { return DmPractitioner; });
/* harmony import */ var _cells_cells_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-element */ "./node_modules/@cells/cells-element/cells-element.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_generic_dp__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-generic-dp */ "./node_modules/@cells-components/cells-generic-dp/cells-generic-dp.js");
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! assert */ "../../.nvm/versions/node/v9.11.2/lib/node_modules/@cells/cells-cli/node_modules/assert/assert.js");
/* harmony import */ var assert__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(assert__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <cells-generic-dp id=\"dp\" host=\"https://obyi7eedtc.execute-api.us-west-2.amazonaws.com/alpha\"> </cells-generic-dp>\n      <cells-generic-dp id=\"dpPokemones\" method=\"GET\"> </cells-generic-dp>\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/

var DmPractitioner =
/*#__PURE__*/
function (_CellsElement) {
  _inherits(DmPractitioner, _CellsElement);

  _createClass(DmPractitioner, null, [{
    key: "is",
    get: function get() {
      return 'dm-practitioner';
    } // Declare properties

  }, {
    key: "properties",
    get: function get() {
      return {
        nextPagePokemones: {
          type: String
        }
      };
    } // Initialize properties

  }]);

  function DmPractitioner() {
    var _this;

    _classCallCheck(this, DmPractitioner);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DmPractitioner).call(this));
    _this.nextPagePokemones = 'https://pokeapi.co/api/v2/pokemon';
    return _this;
  }

  _createClass(DmPractitioner, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(DmPractitioner.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-login', function (data) {
        return _this2._login(data);
      });
      this.subscribe('ch-create-user', function (data) {
        return _this2._createUser(data);
      });
      this.subscribe('ch-get-user-info', function (_) {
        return _this2._getUserData();
      });
      this.subscribe('ch-get-user-movements', function (_) {
        return _this2._getUserMovements();
      });
      this.subscribe('ch-get-pokemones', function (_) {
        return _this2._getPokemones();
      });
      this.subscribe('ch-buy-pokemon', function (body) {
        return _this2._buyPokemon(body);
      });
      this.subscribe('ch-get-user-pokemones', function (_) {
        return _this2._getUserPokemones();
      });
    }
  }, {
    key: "_getUserPokemones",
    value: function _getUserPokemones() {
      var _this3 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
      dp.method = "GET";
      dp.headers = {
        token: sessionStorage.getItem('token')
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200) {
          var newData = detail.data.map(function (movement) {
            return {
              name: movement.details.name,
              image: movement.details.image
            };
          });
          var newDetail = {
            statusCode: 200,
            data: newData
          };

          _this3.publish("ch-get-user-pokemones-response", newDetail);
        } else {
          _this3.publish("ch-get-user-pokemones-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this3.publish("ch-get-user-pokemones-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_buyPokemon",
    value: function _buyPokemon(body) {
      var _this4 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
      dp.method = "POST";
      dp.headers = {
        token: sessionStorage.getItem('token')
      };
      dp.body = {
        data: body
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200 || detail.statusCode == 204) {
          _this4.publish("ch-buy-pokemon-response", detail);
        } else {
          _this4.publish("ch-buy-pokemon-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this4.publish("ch-buy-pokemon-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_getPokemones",
    value: function _getPokemones() {
      var _this5 = this;

      var dp = this.shadowRoot.getElementById('dpPokemones');
      dp.host = this.nextPagePokemones;
      dp.method = "GET";
      dp.generateRequest().then(function (result) {
        _this5.nextPagePokemones = result.response.next;

        _this5._getFullPokemonData(result.response.results);
      }).catch(function (error) {
        _this5.publish("ch-get-pokemones-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_getFullPokemonData",
    value: function _getFullPokemonData(list) {
      var _this6 = this;

      var dp = this.shadowRoot.getElementById('dpPokemones');
      dp.method = "GET";
      list.map(function (pokemon) {
        dp.host = pokemon.url;
        dp.generateRequest().then(function (result) {
          var pokemon = {
            name: result.response.name,
            price: result.response.weight,
            image: result.response.sprites.front_default || result.response.sprites.front_female || result.response.sprites.front_shiny
          };

          _this6.publish("ch-get-pokemones-response", pokemon);
        });
      });
    }
  }, {
    key: "_getUserMovements",
    value: function _getUserMovements() {
      var _this7 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "users/" + sessionStorage.getItem('user') + '/movements';
      dp.method = "GET";
      dp.headers = {
        token: sessionStorage.getItem('token')
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200) {
          var newData = detail.data.map(function (movement) {
            var d = new Date(movement.date);
            var dateForm = "".concat(d.getDay(), "/").concat(d.getMonth() + 1, "/").concat(d.getFullYear(), " - \n            ").concat(d.getHours(), ":").concat(d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
            return {
              amount: movement.amount,
              date: dateForm,
              details: movement.details,
              type: movement.type
            };
          });
          var newDetail = {
            statusCode: 200,
            data: newData
          };

          _this7.publish("ch-get-user-movements-response", newDetail);
        } else {
          _this7.publish("ch-get-user-movements-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this7.publish("ch-get-user-movements-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_getUserData",
    value: function _getUserData() {
      var _this8 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "users/" + sessionStorage.getItem('user');
      dp.method = "GET";
      dp.headers = {
        token: sessionStorage.getItem('token')
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200) {
          _this8.publish("ch-get-user-info-response", detail);
        } else {
          _this8.publish("ch-get-user-info-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this8.publish("ch-get-user-info-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_createUser",
    value: function _createUser(data) {
      var _this9 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "users";
      dp.method = "POST";
      dp.body = {
        user: data.user,
        password: data.pass
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200) {
          _this9.publish("ch-create-user-response", detail);
        } else {
          _this9.publish("ch-create-user-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this9.publish("ch-create-user-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "_login",
    value: function _login(data) {
      var _this10 = this;

      var dp = this.shadowRoot.getElementById('dp');
      dp.path = "login";
      dp.method = "POST";
      dp.body = {
        user: data.user,
        password: data.pass
      };
      dp.generateRequest().then(function (result) {
        var detail = result.response;

        if (detail.statusCode == 200) {
          _this10.publish("ch-login-response", detail);
        } else {
          _this10.publish("ch-login-response", {
            data: "error"
          });
        }
      }).catch(function (error) {
        _this10.publish("ch-login-response", {
          data: "error"
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject());
    }
  }]);

  return DmPractitioner;
}(_cells_cells_element__WEBPACK_IMPORTED_MODULE_0__["CellsElement"]); // Register the element with the browser

customElements.define(DmPractitioner.is, DmPractitioner);

/***/ }),

/***/ "./app/elements/pt-info-label.js":
/*!***************************************!*\
  !*** ./app/elements/pt-info-label.js ***!
  \***************************************/
/*! exports provided: PtInfoLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PtInfoLabel", function() { return PtInfoLabel; });
/* harmony import */ var _cells_cells_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-element */ "./node_modules/@cells/cells-element/cells-element.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _bbva_web_components_bbva_button_default__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @bbva-web-components/bbva-button-default */ "./node_modules/@bbva-web-components/bbva-button-default/bbva-button-default.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject3() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["<div class=\"fullSpiner\">\n              <h3>", "</h3>\n              <bbva-button-default @click=\"", "\">Cerrar</bbva-button-default>\n            </div>"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <style>\n        .fullSpiner {\n          z-index: 998;\n          width: 100vw;\n          height: 100vh;\n          background: rgba(255,255,255,0.9);\n          left: 0;\n          top: 0;\n          position: absolute;\n          display: flex;\n          justify-content: center;\n          align-items: center;\n          flex-direction: column;\n        }\n      </style>\n\n      ", "\n        \n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/

var PtInfoLabel =
/*#__PURE__*/
function (_CellsElement) {
  _inherits(PtInfoLabel, _CellsElement);

  _createClass(PtInfoLabel, null, [{
    key: "is",
    get: function get() {
      return 'pt-info-label';
    } // Declare properties

  }, {
    key: "properties",
    get: function get() {
      return {
        msg: {
          type: String
        },
        show: {
          type: Boolean
        }
      };
    } // Initialize properties

  }]);

  function PtInfoLabel() {
    var _this;

    _classCallCheck(this, PtInfoLabel);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PtInfoLabel).call(this));
    _this.msg = "";
    _this.show = false;
    return _this;
  }

  _createClass(PtInfoLabel, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(PtInfoLabel.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-msg-label', function (data) {
        return _this2._show(data);
      });
    }
  }, {
    key: "_show",
    value: function _show(data) {
      this.show = true;
      this.msg = data;
    }
  }, {
    key: "_hide",
    value: function _hide() {
      this.show = false;
      this.msg = "";
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.show ? Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject2(), this.msg, this._hide) : Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject3()));
    }
  }]);

  return PtInfoLabel;
}(_cells_cells_element__WEBPACK_IMPORTED_MODULE_0__["CellsElement"]); // Register the element with the browser

customElements.define(PtInfoLabel.is, PtInfoLabel);

/***/ }),

/***/ "./app/elements/pt-spinner.js":
/*!************************************!*\
  !*** ./app/elements/pt-spinner.js ***!
  \************************************/
/*! exports provided: PtSpinner */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PtSpinner", function() { return PtSpinner; });
/* harmony import */ var _cells_cells_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-element */ "./node_modules/@cells/cells-element/cells-element.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _bbva_web_components_bbva_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @bbva-web-components/bbva-spinner */ "./node_modules/@bbva-web-components/bbva-spinner/bbva-spinner.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject3() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["<div class=\"fullSpiner\">\n              <bbva-spinner> </bbva-spinner>\n            </div>"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <style>\n        .fullSpiner {\n          z-index: 999;\n          width: 100vw;\n          height: 100vh;\n          background: rgba(55,155,255,0.5);\n          left: 0;\n          top: 0;\n          position: absolute;\n          display: flex;\n          justify-content: center;\n        }\n      </style>\n      \n      \n      ", "\n        \n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




/**
This component ...

Example:

* @customElement
* @polymer
* @LitElement
*/

var PtSpinner =
/*#__PURE__*/
function (_CellsElement) {
  _inherits(PtSpinner, _CellsElement);

  _createClass(PtSpinner, null, [{
    key: "is",
    get: function get() {
      return 'pt-spinner';
    } // Declare properties

  }, {
    key: "properties",
    get: function get() {
      return {
        show: {
          type: Boolean
        }
      };
    } // Initialize properties

  }]);

  function PtSpinner() {
    var _this;

    _classCallCheck(this, PtSpinner);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PtSpinner).call(this));
    _this.show = false;
    return _this;
  }

  _createClass(PtSpinner, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(PtSpinner.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-spinner', function (data) {
        return _this2._toggleSPinner(data);
      });
    }
  }, {
    key: "_toggleSPinner",
    value: function _toggleSPinner(data) {
      this.show = data;
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.show ? Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject2()) : Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject3()));
    }
  }]);

  return PtSpinner;
}(_cells_cells_element__WEBPACK_IMPORTED_MODULE_0__["CellsElement"]); // Register the element with the browser

customElements.define(PtSpinner.is, PtSpinner);

/***/ }),

/***/ "./app/pages/home-page/home-page-styles.js":
/*!*************************************************!*\
  !*** ./app/pages/home-page/home-page-styles.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n.container {\n  margin-top: 1.5rem;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


/* harmony default export */ __webpack_exports__["default"] = (Object(lit_element__WEBPACK_IMPORTED_MODULE_0__["css"])(_templateObject()));

/***/ }),

/***/ "./app/pages/home-page/home-page.js":
/*!******************************************!*\
  !*** ./app/pages/home-page/home-page.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cells_cells_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-page */ "./node_modules/@cells/cells-page/cells-page.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_template_paper_drawer_panel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-template-paper-drawer-panel */ "./node_modules/@cells-components/cells-template-paper-drawer-panel/cells-template-paper-drawer-panel.js");
/* harmony import */ var _bbva_web_components_bbva_header_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bbva-web-components/bbva-header-main */ "./node_modules/@bbva-web-components/bbva-header-main/bbva-header-main.js");
/* harmony import */ var _home_page_styles_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home-page-styles.js */ "./app/pages/home-page/home-page-styles.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    <style>", "</style>\n    <link rel=\"stylesheet\" href=\"../../resources/css/bootstrap4.3.1.min.css\">\n    <cells-template-paper-drawer-panel mode=\"seamed\">\n\n      <div slot=\"app__header\">\n        <bbva-header-main text=", "></bbva-header-main>\n        <practitioner-datamanagers id=\"id\"></practitioner-datamanagers>\n      </div>\n\n      <div slot=\"app__main\">\n        <div class=\"container\">\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-9\">\n              <input type=\"text\" id=\"usr\" class=\"form-control\" @input=", " placeholder=\"Usuario\">\n            </div>\n            <div class=\"col-9 d-sm-none\">&nbsp;</div>\n            <div class=\"col-9\">\n              <input type=\"password\" id=\"pss\" class=\"form-control\" @input=", " placeholder=\"Contrase\xF1a\">\n            </div>\n          </div>\n          <hr>\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-success btn-lg btn-block\" @click=\"", "\">Login</button>\n              <br>\n              <button type=\"button\" class=\"btn-block btn-link\" @click=\"", "\">Registrate gratis</button>\n            </div>\n          </div>\n        </div>\n        \n      </div>\n\n     </cells-template-paper-drawer-panel>"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var HomePage =
/*#__PURE__*/
function (_CellsPage) {
  _inherits(HomePage, _CellsPage);

  _createClass(HomePage, null, [{
    key: "is",
    get: function get() {
      return 'home-page';
    }
  }]);

  function HomePage() {
    var _this;

    _classCallCheck(this, HomePage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HomePage).call(this));
    _this.headerTitle = 'Inicia sesion';
    return _this;
  }

  _createClass(HomePage, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(HomePage.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-login-response', function (data) {
        return _this2._loginResponde(data);
      });
    }
  }, {
    key: "_loginResponde",
    value: function _loginResponde(data) {
      if (data.data == "error") {
        this.publish('ch-msg-label', "Datos incorrectos");
      } else {
        sessionStorage.setItem('user', data.user);
        sessionStorage.setItem('token', data.detail);
        this.navigate('movements');
      }

      this.publish('ch-spinner', false);
    }
  }, {
    key: "onPageEnter",
    value: function onPageEnter() {
      //console.log('Page loaded');
      sessionStorage.removeItem('user');
      sessionStorage.removeItem('token');
      this.user = '';
      this.password = '';
      this.shadowRoot.getElementById('usr').value = '';
      this.shadowRoot.getElementById('pss').value = '';
    }
  }, {
    key: "onPageLeave",
    value: function onPageLeave() {//console.log('Page unloaded');
    }
  }, {
    key: "_singIn",
    value: function _singIn() {
      this.navigate('singin', {});
    }
  }, {
    key: "_login",
    value: function _login() {
      //console.log(this.user, this.password)
      var user = this.user,
          pass = this.password;
      this.publish('ch-login', {
        user: user,
        pass: pass
      });
      this.publish('ch-spinner', true); //this.navigate('another', { title: 'This is another page' });
    }
  }, {
    key: "handleUser",
    value: function handleUser(e) {
      this.user = e.target.value; //this.shadowRoot.getElementById('id').getAlert(e.target.value);
    }
  }, {
    key: "handlePass",
    value: function handlePass(e) {
      this.password = e.target.value;
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.constructor.shadyStyles, this.headerTitle, this.handleUser, this.handlePass, this._login, this._singIn);
    }
  }], [{
    key: "properties",
    get: function get() {
      return {
        user: {
          type: String
        },
        password: {
          type: String
        },
        headerTitle: {
          type: String
        }
      };
    }
  }, {
    key: "shadyStyles",
    get: function get() {
      return "\n      ".concat(_home_page_styles_js__WEBPACK_IMPORTED_MODULE_4__["default"].cssText, "\n    ");
    }
  }]);

  return HomePage;
}(_cells_cells_page__WEBPACK_IMPORTED_MODULE_0__["CellsPage"]);

window.customElements.define(HomePage.is, HomePage);

/***/ }),

/***/ "./app/pages/movements-page/movements-page-styles.js":
/*!***********************************************************!*\
  !*** ./app/pages/movements-page/movements-page-styles.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n.container {\n  margin-top: 1.5rem;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


/* harmony default export */ __webpack_exports__["default"] = (Object(lit_element__WEBPACK_IMPORTED_MODULE_0__["css"])(_templateObject()));

/***/ }),

/***/ "./app/pages/movements-page/movements-page.js":
/*!****************************************************!*\
  !*** ./app/pages/movements-page/movements-page.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cells_cells_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-page */ "./node_modules/@cells/cells-page/cells-page.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_template_paper_drawer_panel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-template-paper-drawer-panel */ "./node_modules/@cells-components/cells-template-paper-drawer-panel/cells-template-paper-drawer-panel.js");
/* harmony import */ var _bbva_web_components_bbva_header_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bbva-web-components/bbva-header-main */ "./node_modules/@bbva-web-components/bbva-header-main/bbva-header-main.js");
/* harmony import */ var _movements_page_styles_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./movements-page-styles.js */ "./app/pages/movements-page/movements-page-styles.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n              <div class=\"row justify-content-center\">\n                <div class=\"col-10\" style=\"border: solid gray 1.5px\">\n                  <div class=\"col-12\">\n                    <h4>", "</h4>\n                  </div>\n                  <div class=\"row\">\n                    <div class=\"col-6\">\n                      Fecha\n                    </div>\n                    <div class=\"col-6\">\n                      ", "\n                    </div>\n                  </div>\n                  <div class=\"row\">\n                    <div class=\"col-6\">\n                      Monto\n                    </div>\n                    <div class=\"col-6\">\n                      $", "\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <br><br>\n            "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    <style>", "</style>\n    <link rel=\"stylesheet\" href=\"../../resources/css/bootstrap4.3.1.min.css\">\n    <cells-template-paper-drawer-panel mode=\"seamed\">\n\n      <div slot=\"app__header\">\n        <bbva-header-main text=", "></bbva-header-main>\n        <practitioner-datamanagers id=\"id\"></practitioner-datamanagers>\n      </div>\n\n      <div slot=\"app__main\">\n        <div class=\"container\">\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-4\">\n              <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" @click=\"", "\">Comprar pokemones</button>\n            </div>\n            <div class=\"col-4\">\n              <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" @click=\"", "\">Mis pokemones</button>\n            </div>\n          </div>\n\n          <br><br>\n\n          <div class=\"row justify-content-center\">\n            <h1 style=\"color: green\">$", "</h1>\n          </div>\n\n          ", "\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-danger btn-lg btn-block\" @click=\"", "\">Salir</button>\n            </div>\n          </div>\n\n            \n        </div>\n        \n      </div>\n\n     </cells-template-paper-drawer-panel>"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var MovementsPage =
/*#__PURE__*/
function (_CellsPage) {
  _inherits(MovementsPage, _CellsPage);

  _createClass(MovementsPage, null, [{
    key: "is",
    get: function get() {
      return 'movements-page';
    }
  }]);

  function MovementsPage() {
    var _this;

    _classCallCheck(this, MovementsPage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(MovementsPage).call(this));
    _this.headerTitle = 'Movimientos';
    _this.balance = 0;
    _this.movements = [];
    return _this;
  }

  _createClass(MovementsPage, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(MovementsPage.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-get-user-info-response', function (data) {
        return _this2._getUserInfo(data);
      });
      this.subscribe('ch-get-user-movements-response', function (data) {
        return _this2._getUserMovements(data);
      });
    }
  }, {
    key: "_getUserInfo",
    value: function _getUserInfo(data) {
      if (data.data == "error") {
        this.publish('ch-msg-label', "Error inesperado");
      } else {
        this.balance = data.data;
      }
    }
  }, {
    key: "_getUserMovements",
    value: function _getUserMovements(movements) {
      if (movements.data == "error") {
        this.publish('ch-msg-label', "Error inesperado");
      } else {
        this.movements = movements.data;
      }

      this.publish('ch-spinner', false);
    }
  }, {
    key: "onPageEnter",
    value: function onPageEnter() {
      //console.log('Page loaded');
      this.publish('ch-spinner', true);
      this.publish('ch-get-user-info');
      this.publish('ch-get-user-movements');
    }
  }, {
    key: "onPageLeave",
    value: function onPageLeave() {//console.log('Page unloaded');
    }
  }, {
    key: "_return",
    value: function _return() {
      this.navigate('home', {});
    }
  }, {
    key: "_singin",
    value: function _singin() {
      console.log(this.user, this.password, this.password2); //this.publish('from-channel', { from: 'home' });
      //this.navigate('another', { title: 'This is another page' });
    }
  }, {
    key: "handleUser",
    value: function handleUser(e) {
      this.user = e.target.value; //this.shadowRoot.getElementById('id').getAlert(e.target.value);
    }
  }, {
    key: "handlePass",
    value: function handlePass(e) {
      this.password = e.target.value;
    }
  }, {
    key: "handlePass2",
    value: function handlePass2(e) {
      this.password2 = e.target.value;
    }
  }, {
    key: "getDate",
    value: function getDate(time) {
      var d = new Date();
      return "".concat(d.getDay(time), "/").concat(d.getMonth(time) + 1, "/").concat(d.getFullYear(time), " - \n            ").concat(d.getHours(time), ":").concat(d.getMinutes(time) < 10 ? '0' + d.getMinutes(time) : d.getMinutes(time));
    }
  }, {
    key: "_logout",
    value: function _logout() {
      this.navigate('home');
    }
  }, {
    key: "_goList",
    value: function _goList() {
      this.navigate('pokemones');
    }
  }, {
    key: "_goMyPokemones",
    value: function _goMyPokemones() {
      this.navigate('userpokemones');
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.constructor.shadyStyles, this.headerTitle, this._goList, this._goMyPokemones, this.balance, this.movements.map(function (movement) {
        return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject2(), movement.type, movement.date, movement.amount);
      }), this._logout);
    }
  }], [{
    key: "properties",
    get: function get() {
      return {
        headerTitle: {
          type: String
        },
        balance: {
          type: Number
        },
        movements: {
          type: Array
        }
      };
    }
  }, {
    key: "shadyStyles",
    get: function get() {
      return "\n      ".concat(_movements_page_styles_js__WEBPACK_IMPORTED_MODULE_4__["default"].cssText, "\n    ");
    }
  }]);

  return MovementsPage;
}(_cells_cells_page__WEBPACK_IMPORTED_MODULE_0__["CellsPage"]);

window.customElements.define(MovementsPage.is, MovementsPage);

/***/ }),

/***/ "./app/pages/pokemones-page/pokemones-page-styles.js":
/*!***********************************************************!*\
  !*** ./app/pages/pokemones-page/pokemones-page-styles.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n.container {\n  margin-top: 1.5rem;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


/* harmony default export */ __webpack_exports__["default"] = (Object(lit_element__WEBPACK_IMPORTED_MODULE_0__["css"])(_templateObject()));

/***/ }),

/***/ "./app/pages/pokemones-page/pokemones-page.js":
/*!****************************************************!*\
  !*** ./app/pages/pokemones-page/pokemones-page.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cells_cells_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-page */ "./node_modules/@cells/cells-page/cells-page.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_template_paper_drawer_panel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-template-paper-drawer-panel */ "./node_modules/@cells-components/cells-template-paper-drawer-panel/cells-template-paper-drawer-panel.js");
/* harmony import */ var _bbva_web_components_bbva_header_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bbva-web-components/bbva-header-main */ "./node_modules/@bbva-web-components/bbva-header-main/bbva-header-main.js");
/* harmony import */ var _pokemones_page_styles_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pokemones-page-styles.js */ "./app/pages/pokemones-page/pokemones-page-styles.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n            <div class=\"col-10 col-md-3\" style=\"padding-bottom: 1rem;\" >\n              <div style=\"border: solid gray 2px\">\n                <div class=\"col-12\" style=\"text-align: center\">\n                  <div style=\"display: flex; width: 100%; justify-content: center;\">\n                    <div style=\"width: 96px; height: 96px; background: url(", ")\"></div>\n                  </div>\n                  <br>\n                  <h3>", "</h3>\n                </div>\n                <br>\n                <div class=\"row justify-content-center\">\n                  <div class=\"col-7\">\n                    <button type=\"button\" class=\"btn btn-success btn-lg btn-block\" @click=\"", "\">Comprar $", "</button>\n                  </div>\n                </div>\n                <br>\n              </div>\n            </div>\n            "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    <style>", "</style>\n    <link rel=\"stylesheet\" href=\"../../resources/css/bootstrap4.3.1.min.css\">\n    <cells-template-paper-drawer-panel mode=\"seamed\">\n\n      <div slot=\"app__header\">\n        <bbva-header-main text=", "></bbva-header-main>\n        <practitioner-datamanagers id=\"id\"></practitioner-datamanagers>\n      </div>\n\n      <div slot=\"app__main\">\n        <div class=\"container\">\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" @click=\"", "\">Ver movimientos</button>\n            </div>\n          </div>\n\n          <br><br>\n\n          <div class=\"row justify-content-center\">\n            <h1 style=\"color: green\">$", "</h1>\n          </div>\n\n          <div class=\"row justify-content-center\">\n\n          ", "\n            \n          </div>\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" @click=\"", "\">Mas pokemones</button>\n              <br><br>\n              <button type=\"button\" class=\"btn btn-danger btn-lg btn-block\" @click=\"", "\">Salir</button>\n            </div>\n          </div>\n\n            \n        </div>\n        \n      </div>\n\n     </cells-template-paper-drawer-panel>"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var PokemonesPage =
/*#__PURE__*/
function (_CellsPage) {
  _inherits(PokemonesPage, _CellsPage);

  _createClass(PokemonesPage, null, [{
    key: "is",
    get: function get() {
      return 'pokemones-page';
    }
  }]);

  function PokemonesPage() {
    var _this;

    _classCallCheck(this, PokemonesPage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PokemonesPage).call(this));
    _this.headerTitle = 'Tienda pokemon';
    _this.balance = 0;
    _this.pokemones = [];
    _this.tempPokemones = [];
    _this.count = 0;
    return _this;
  }

  _createClass(PokemonesPage, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(PokemonesPage.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-get-user-info-response', function (data) {
        return _this2._getUserInfo(data);
      });
      this.subscribe('ch-get-pokemones-response', function (data) {
        return _this2._getPokemones(data);
      });
      this.subscribe('ch-buy-pokemon-response', function (data) {
        return _this2._buyPokemonResponse(data);
      });
    }
  }, {
    key: "_buyPokemonResponse",
    value: function _buyPokemonResponse(data) {
      if (data.data == "error") {
        this.publish('ch-msg-label', "Error inesperado");
      } else if (data.statusCode == 204) {
        this.publish('ch-msg-label', "Fondos insuficientes");
      } else {
        this.publish('ch-msg-label', "Compra exitosa");
        this.publish('ch-get-user-info');
      }

      this.publish('ch-spinner', false);
    }
  }, {
    key: "_getUserInfo",
    value: function _getUserInfo(data) {
      if (data.data == "error") {
        this.publish('ch-msg-label', "Error inesperado");
      } else {
        this.balance = data.data;
      }
    }
  }, {
    key: "_getPokemones",
    value: function _getPokemones(data) {
      var _this3 = this;

      if (this.count < 19) {
        this.tempPokemones.push(data);
        this.count++;
      } else {
        this.pokemones.map(function (pokemon) {
          _this3.tempPokemones.unshift(pokemon);
        });
        this.pokemones = this.tempPokemones;
        this.tempPokemones = [];
        this.count = 0;
        this.publish('ch-spinner', false);
      }
    }
  }, {
    key: "_buy",
    value: function _buy(pokemon) {
      var date = new Date();
      var body = {
        "amount": pokemon.price,
        "type": "Compra",
        "details": pokemon,
        "date": date.getTime()
      };
      this.publish('ch-buy-pokemon', body);
      this.publish('ch-spinner', true);
    }
  }, {
    key: "_getMorePokemones",
    value: function _getMorePokemones() {
      this.publish('ch-spinner', true);
      this.publish('ch-get-pokemones');
    }
  }, {
    key: "onPageEnter",
    value: function onPageEnter() {
      this.publish('ch-spinner', true);
      this.publish('ch-get-user-info');
      this.publish('ch-get-pokemones');
    }
  }, {
    key: "_logout",
    value: function _logout() {
      this.navigate('home');
    }
  }, {
    key: "_goMovements",
    value: function _goMovements() {
      this.navigate("movements");
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.constructor.shadyStyles, this.headerTitle, this._goMovements, this.balance, this.pokemones.map(function (pokemon) {
        return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject2(), pokemon.image, pokemon.name, function () {
          return _this4._buy(pokemon);
        }, pokemon.price);
      }), this._getMorePokemones, this._logout);
    }
  }], [{
    key: "properties",
    get: function get() {
      return {
        balance: {
          type: Number
        },
        count: {
          type: Number
        },
        headerTitle: {
          type: String
        },
        pokemones: {
          type: Array
        },
        tempPokemones: {
          type: Array
        }
      };
    }
  }, {
    key: "shadyStyles",
    get: function get() {
      return "\n      ".concat(_pokemones_page_styles_js__WEBPACK_IMPORTED_MODULE_4__["default"].cssText, "\n    ");
    }
  }]);

  return PokemonesPage;
}(_cells_cells_page__WEBPACK_IMPORTED_MODULE_0__["CellsPage"]);

window.customElements.define(PokemonesPage.is, PokemonesPage);

/***/ }),

/***/ "./app/pages/singin-page/singin-page-styles.js":
/*!*****************************************************!*\
  !*** ./app/pages/singin-page/singin-page-styles.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n.container {\n  margin-top: 1.5rem;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


/* harmony default export */ __webpack_exports__["default"] = (Object(lit_element__WEBPACK_IMPORTED_MODULE_0__["css"])(_templateObject()));

/***/ }),

/***/ "./app/pages/singin-page/singin-page.js":
/*!**********************************************!*\
  !*** ./app/pages/singin-page/singin-page.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cells_cells_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-page */ "./node_modules/@cells/cells-page/cells-page.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_template_paper_drawer_panel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-template-paper-drawer-panel */ "./node_modules/@cells-components/cells-template-paper-drawer-panel/cells-template-paper-drawer-panel.js");
/* harmony import */ var _bbva_web_components_bbva_header_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bbva-web-components/bbva-header-main */ "./node_modules/@bbva-web-components/bbva-header-main/bbva-header-main.js");
/* harmony import */ var _singin_page_styles_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./singin-page-styles.js */ "./app/pages/singin-page/singin-page-styles.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    <style>", "</style>\n    <link rel=\"stylesheet\" href=\"../../resources/css/bootstrap4.3.1.min.css\">\n    <cells-template-paper-drawer-panel mode=\"seamed\">\n\n      <div slot=\"app__header\">\n        <bbva-header-main text=", "></bbva-header-main>\n        <practitioner-datamanagers id=\"id\"></practitioner-datamanagers>\n      </div>\n\n      <div slot=\"app__main\">\n        <div class=\"container\">\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-9\">\n              <input type=\"text\" class=\"form-control\" @input=", " placeholder=\"Usuario\">\n            </div>\n            <div class=\"col-9 d-sm-none\">&nbsp;</div>\n            <div class=\"col-9\">\n              <input type=\"password\" class=\"form-control\" @input=", " placeholder=\"Contrase\xF1a\">\n            </div>\n            <div class=\"col-9 d-sm-none\">&nbsp;</div>\n            <div class=\"col-9\">\n              <input type=\"password\" class=\"form-control\" @input=", " placeholder=\"Repetir contrase\xF1a\">\n            </div>\n          </div>\n          <hr>\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-success btn-lg btn-block\" @click=\"", "\">Registrarse</button>\n              <br>\n              <button type=\"button\" class=\"btn-block btn-link\" @click=\"", "\">Regresar</button>\n            </div>\n          </div>\n        </div>\n        \n      </div>\n\n     </cells-template-paper-drawer-panel>"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var SinginPage =
/*#__PURE__*/
function (_CellsPage) {
  _inherits(SinginPage, _CellsPage);

  _createClass(SinginPage, null, [{
    key: "is",
    get: function get() {
      return 'singin-page';
    }
  }]);

  function SinginPage() {
    var _this;

    _classCallCheck(this, SinginPage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SinginPage).call(this));
    _this.headerTitle = 'Registrate gratis';
    return _this;
  }

  _createClass(SinginPage, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(SinginPage.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-create-user-response', function (data) {
        return _this2._createUserResponse(data);
      });
    }
  }, {
    key: "onPageEnter",
    value: function onPageEnter() {//console.log('Page loaded');
    }
  }, {
    key: "onPageLeave",
    value: function onPageLeave() {//console.log('Page unloaded');
    }
  }, {
    key: "_return",
    value: function _return() {
      this.navigate('home', {});
    }
  }, {
    key: "_createUserResponse",
    value: function _createUserResponse(data) {
      if (data.data === "error") {
        this.publish('ch-msg-label', "El usuario " + this.user + " ya existe");
      } else {
        this.publish('ch-msg-label', "Usuario creado con exito");
        this.navigate('home', {});
      }

      this.publish('ch-spinner', false);
    }
  }, {
    key: "_singin",
    value: function _singin() {
      if (this.password === this.password2) {
        this.publish('ch-spinner', true);
        this.publish('ch-create-user', {
          user: this.user,
          pass: this.password
        });
      } else {
        this.publish('ch-msg-label', "Las contraseñas no coinciden");
      }
    }
  }, {
    key: "handleUser",
    value: function handleUser(e) {
      this.user = e.target.value; //this.shadowRoot.getElementById('id').getAlert(e.target.value);
    }
  }, {
    key: "handlePass",
    value: function handlePass(e) {
      this.password = e.target.value;
    }
  }, {
    key: "handlePass2",
    value: function handlePass2(e) {
      this.password2 = e.target.value;
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.constructor.shadyStyles, this.headerTitle, this.handleUser, this.handlePass, this.handlePass2, this._singin, this._return);
    }
  }], [{
    key: "properties",
    get: function get() {
      return {
        user: {
          type: String
        },
        password: {
          type: String
        },
        password2: {
          type: String
        },
        headerTitle: {
          type: String
        }
      };
    }
  }, {
    key: "shadyStyles",
    get: function get() {
      return "\n      ".concat(_singin_page_styles_js__WEBPACK_IMPORTED_MODULE_4__["default"].cssText, "\n    ");
    }
  }]);

  return SinginPage;
}(_cells_cells_page__WEBPACK_IMPORTED_MODULE_0__["CellsPage"]);

window.customElements.define(SinginPage.is, SinginPage);

/***/ }),

/***/ "./app/pages/userpokemones-page/userpokemones-page-styles.js":
/*!*******************************************************************!*\
  !*** ./app/pages/userpokemones-page/userpokemones-page-styles.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n.container {\n  margin-top: 1.5rem;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }


/* harmony default export */ __webpack_exports__["default"] = (Object(lit_element__WEBPACK_IMPORTED_MODULE_0__["css"])(_templateObject()));

/***/ }),

/***/ "./app/pages/userpokemones-page/userpokemones-page.js":
/*!************************************************************!*\
  !*** ./app/pages/userpokemones-page/userpokemones-page.js ***!
  \************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cells_cells_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @cells/cells-page */ "./node_modules/@cells/cells-page/cells-page.js");
/* harmony import */ var lit_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lit-element */ "./node_modules/lit-element/lit-element.js");
/* harmony import */ var _cells_components_cells_template_paper_drawer_panel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @cells-components/cells-template-paper-drawer-panel */ "./node_modules/@cells-components/cells-template-paper-drawer-panel/cells-template-paper-drawer-panel.js");
/* harmony import */ var _bbva_web_components_bbva_header_main__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @bbva-web-components/bbva-header-main */ "./node_modules/@bbva-web-components/bbva-header-main/bbva-header-main.js");
/* harmony import */ var _userpokemones_page_styles_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./userpokemones-page-styles.js */ "./app/pages/userpokemones-page/userpokemones-page-styles.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n            <div class=\"col-10 col-md-3\" style=\"padding-bottom: 1rem;\" >\n              <div style=\"border: solid gray 2px\">\n                <div class=\"col-12\" style=\"text-align: center\">\n                  <div style=\"display: flex; width: 100%; justify-content: center;\">\n                    <div style=\"width: 96px; height: 96px; background: url(", ")\"></div>\n                  </div>\n                  <br>\n                  <h3>", "</h3>\n                </div>\n                <br>\n              </div>\n            </div>\n            "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    <style>", "</style>\n    <link rel=\"stylesheet\" href=\"../../resources/css/bootstrap4.3.1.min.css\">\n    <cells-template-paper-drawer-panel mode=\"seamed\">\n\n      <div slot=\"app__header\">\n        <bbva-header-main text=", "></bbva-header-main>\n        <practitioner-datamanagers id=\"id\"></practitioner-datamanagers>\n      </div>\n\n      <div slot=\"app__main\">\n        <div class=\"container\">\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" @click=\"", "\">Ver movimientos</button>\n            </div>\n          </div>\n\n          <br><br>\n\n          <div class=\"row justify-content-center\">\n            <h1 style=\"color: green\">Mis pokemones</h1>\n          </div>\n\n          <div class=\"row justify-content-center\">\n\n          ", "\n            \n          </div>\n\n          <div class=\"row justify-content-center\">\n            <div class=\"col-8\">\n              <button type=\"button\" class=\"btn btn-danger btn-lg btn-block\" @click=\"", "\">Salir</button>\n            </div>\n          </div>\n\n            \n        </div>\n        \n      </div>\n\n     </cells-template-paper-drawer-panel>"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var UserpokemonesPage =
/*#__PURE__*/
function (_CellsPage) {
  _inherits(UserpokemonesPage, _CellsPage);

  _createClass(UserpokemonesPage, null, [{
    key: "is",
    get: function get() {
      return 'userpokemones-page';
    }
  }]);

  function UserpokemonesPage() {
    var _this;

    _classCallCheck(this, UserpokemonesPage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(UserpokemonesPage).call(this));
    _this.headerTitle = 'Mis pokemon';
    _this.pokemones = [];
    return _this;
  }

  _createClass(UserpokemonesPage, [{
    key: "firstUpdated",
    value: function firstUpdated() {
      _get(_getPrototypeOf(UserpokemonesPage.prototype), "firstUpdated", this).call(this);

      this._handleConnections();
    }
  }, {
    key: "_handleConnections",
    value: function _handleConnections() {
      var _this2 = this;

      this.subscribe('ch-get-user-pokemones-response', function (data) {
        return _this2._getUserPokemones(data);
      });
    }
  }, {
    key: "_getUserPokemones",
    value: function _getUserPokemones(pokemones) {
      if (pokemones.data == "error") {
        this.publish('ch-msg-label', "Error inesperado");
      } else {
        this.pokemones = pokemones.data;
      }

      this.publish('ch-spinner', false);
    }
  }, {
    key: "onPageEnter",
    value: function onPageEnter() {
      this.publish('ch-spinner', true);
      this.publish('ch-get-user-pokemones');
    }
  }, {
    key: "_logout",
    value: function _logout() {
      this.navigate('home');
    }
  }, {
    key: "_goMovements",
    value: function _goMovements() {
      this.navigate("movements");
    }
  }, {
    key: "render",
    value: function render() {
      return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject(), this.constructor.shadyStyles, this.headerTitle, this._goMovements, this.pokemones.map(function (pokemon) {
        return Object(lit_element__WEBPACK_IMPORTED_MODULE_1__["html"])(_templateObject2(), pokemon.image, pokemon.name);
      }), this._logout);
    }
  }], [{
    key: "properties",
    get: function get() {
      return {
        headerTitle: {
          type: String
        },
        pokemones: {
          type: Array
        }
      };
    }
  }, {
    key: "shadyStyles",
    get: function get() {
      return "\n      ".concat(_userpokemones_page_styles_js__WEBPACK_IMPORTED_MODULE_4__["default"].cssText, "\n    ");
    }
  }]);

  return UserpokemonesPage;
}(_cells_cells_page__WEBPACK_IMPORTED_MODULE_0__["CellsPage"]);

window.customElements.define(UserpokemonesPage.is, UserpokemonesPage);

/***/ }),

/***/ "./app/scripts/lit-initial-components.js":
/*!***********************************************!*\
  !*** ./app/scripts/lit-initial-components.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _elements_dm_practitioner_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../elements/dm-practitioner.js */ "./app/elements/dm-practitioner.js");
/* harmony import */ var _elements_pt_spinner_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../elements/pt-spinner.js */ "./app/elements/pt-spinner.js");
/* harmony import */ var _elements_pt_info_label_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../elements/pt-info-label.js */ "./app/elements/pt-info-label.js");
/* harmony import */ var _pages_home_page_home_page_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pages//home-page/home-page.js */ "./app/pages/home-page/home-page.js");
/* harmony import */ var _pages_movements_page_movements_page_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../pages//movements-page/movements-page.js */ "./app/pages/movements-page/movements-page.js");
/* harmony import */ var _pages_pokemones_page_pokemones_page_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pages//pokemones-page/pokemones-page.js */ "./app/pages/pokemones-page/pokemones-page.js");
/* harmony import */ var _pages_singin_page_singin_page_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../pages//singin-page/singin-page.js */ "./app/pages/singin-page/singin-page.js");
/* harmony import */ var _pages_userpokemones_page_userpokemones_page_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pages//userpokemones-page/userpokemones-page.js */ "./app/pages/userpokemones-page/userpokemones-page.js");
// Import here your LitElement initial components (critical / startup)



// Auto generated imports below. DO NOT remove!
// will be replaced with imports
// ${filledByCellsWithAutoImports}






/***/ })

/******/ });
//# sourceMappingURL=lit-initial-components-._app_e.js.map